<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
	<p class="sucessmsg">
    <?php echo $this->session->flashdata('msg_success'); ?>
</p>
    <div class="fadeIn first">
     <h3>Login Here</h3>
    </div>

    <!-- Login Form -->
    <form id="loginform" method="post">
      <input type="text" id="login" class="fadeIn second" name="login" placeholder="login" required>
      <input type="password" id="password" class="fadeIn third" name="password" placeholder="password" required>
      <input type="submit" class="fadeIn fourth" value="Log In">
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
      <a class="underlineHover" href="register">Register Here</a>
    </div>
<p class="errormsg">
    <?php echo $this->session->flashdata('msg_error'); ?>
</p>
  </div>
</div>