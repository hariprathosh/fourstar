<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
     <h3>Regiser Here</h3>
    </div>

    <!-- Register Form -->
    <form id="loginform" method="post" action="reginsert">
      <input type="text" id="name" class="fadeIn second" name="name" placeholder="Name" required>	   
	  <input type="email" id="email" class="fadeIn third" name="email" placeholder="Email ID" required>
	  <input type="text" id="username" class="fadeIn second" name="username" placeholder="Username" required>
      <input type="password" id="password" class="fadeIn third" name="password" placeholder="password" required>
	  <select name="city" id="city" required>
		<option value="">City</option>
		<option value="1">Chennai</option>
		<option value="2">Delhi</option>
		<option value="3">Mumbai</option>
	   </select>
      <input type="submit" class="fadeIn fourth" value="Log In">
    </form>
<p class="errormsg">
    <?php echo $this->session->flashdata('msg_error'); ?>
</p>
  </div>
</div>