<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Users extends CI_Controller { 
     
    function __construct() { 
        parent::__construct();          
        // Load form validation ibrary & user model 
        $this->load->library(array('session', 'form_validation'));
        $this->load->model('usersmodel'); 
        $this->load->helper(array('form', 'url')); 
        $this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn'); 
    } 
	
	public function register()
    {   
		$this->load->view('elements/header'); 
		$this->load->view('register'); 
	    $this->load->view('elements/footer');		
	}
    public function login()
    {        
	if (!$this->session->userdata('is_logged_in')) {
		
		$this->load->view('elements/header'); 
		$this->load->view('login'); 
	    $this->load->view('elements/footer');
		
	}else{
		
        $username = $this->input->post('login');
        $password = $this->input->post('password');
        if ($this->input->post('login') == "" || $this->input->post('password') =="")
        {            
            $this->load->view('elements/header'); 
			$this->load->view('login'); 
			$this->load->view('elements/footer');
 
        }
        else
        { 
            if ($user = $this->usersmodel->get_user_login($username, $password))
            {                  
                $this->session->set_userdata('name', $user['Name']);
                $this->session->set_userdata('user_id', $user['id']);
                $this->session->set_userdata('is_logged_in', true);
                
                
                $this->session->set_flashdata('msg_success','Login Successful!');
                redirect('hotelsearch');                
            }
            else
            {
                $this->session->set_flashdata('msg_error','Login credentials does not match!');
                redirect('users/login');
            }
        }
     // Load view 
	} 
    } 
 
    public function reginsert(){ 
        $data = $userData = array(); 
         
        // If registration request is submitted 
        if($this->input->post('email')){ 
            
            $userData = array( 
                'username' => strip_tags($this->input->post('username')), 
                'name' => strip_tags($this->input->post('name')), 
                'email' => strip_tags($this->input->post('email')), 
                'password' => md5($this->input->post('password')), 
                'city' => $this->input->post('city'), 
                'status' => '1',
				'date_created'=>date('Y-m-d H:i:s'),
				'date_modified'=>date('Y-m-d H:i:s')
            ); 
			
                $insert = $this->usersmodel->set_user($userData); 
                if($insert){ 
                     $this->session->set_flashdata('msg_success', 'Your account registration has been successful. Please login to your account.'); 
                    redirect('users/login'); 
                }else{ 
                    $data['error_msg'] = 'Some problems occured, please try again.'; 
                } 
        } else{ 
                $data['error_msg'] = 'Please fill all the mandatory fields.'; 
            }  
         
        // Posted data 
        $data['user'] = $userData; 
         
        // Load view 
        $this->load->view('elements/header'); 
        $this->load->view('users/registration'); 
        $this->load->view('elements/footer'); 
    } 
     
    public function logout(){ 
        $this->session->unset_userdata('isUserLoggedIn'); 
        $this->session->unset_userdata('userId'); 
        $this->session->sess_destroy(); 
        redirect('users/login'); 
    }  
}