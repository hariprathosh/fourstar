<?php
class Usersmodel extends CI_Model {
 
    public function __construct()
    {
        $this->load->database();
    }
	
    public function get_user_login($username, $password)
    {
        $query = $this->db->get_where('users', array('username' => $username, 'password' => md5($password),'status'=>1));        
        //return $query->num_rows();
        return $query->row_array();
    }
    
    public function set_user($data)
    {
        return $this->db->insert('users', $data);
        
    }
    
    public function delete_user($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('user');
    }    
    
}